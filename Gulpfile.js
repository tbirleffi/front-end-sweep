'use strict'

var gulp = require('gulp');
var nodemon = require('gulp-nodemon');
var browserSync = require('browser-sync');
var sass = require('gulp-sass');
var minifycss = require('gulp-minify-css');

var isSynced = false;
var port = 4000;

gulp.task('styles', function() {
    gulp.src('sass/*.scss')
        .pipe(sass({errLogToConsole: true}))
        .pipe(gulp.dest('public/assets/css'))
        .pipe(minifycss())
        .pipe(gulp.dest('public/assets/css'));
});

gulp.task('browser-sync', function() {
    browserSync({
        proxy: "http://localhost:"+port,
        files: ["public/*.html", "public/assets/css/*.css"],
        logConnections: true,
        logLevel: 'debug',
    });
});

gulp.task('develop', function () {
    nodemon({ script: 'server.js' })
    .on('start', function() {
        console.log('started!');
        if(!isSynced) {
            setTimeout(function() { 
                gulp.start('browser-sync');
                isSynced = true;
            }, 2000);
        }
    })
    .on('change', function() {
        console.log('changed!');
    })
    .on('restart', function () {
        console.log('restarted!');
    })
    .on('crash', function () {
        console.log('crash!');
        isSynced = false;
    });
});

gulp.task('watch', function() {
    gulp.watch('sass/includes/*.scss', ['styles']);
    gulp.watch('sass/*.scss', ['styles']);
});

gulp.task('default', ['develop', 'watch']);

