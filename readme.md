# Front-End Sweep
# TODO: Test in IE9+, that's it.

### Make sure node is installed.
```
https://nodejs.org/download/
```

### Install the dependecies.
```
npm install
```

### NOTE: If you have sudo permissions issue with NPM, run this:
```
sudo chown -R $(whoami) ~/.npm
```

### To run the server for develop.
```
gulp
```

### View website here.
```
http://localhost:3000
```