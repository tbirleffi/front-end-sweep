// Setup.
var express = require('express');
var app = express();
var server = require('http').createServer(app);
var port = 4000;

// Set the root public directory.
app.use(express.static(__dirname + '/public'));

// Serve the HTML.
app.get('/', function(req, res) {
	res.sendFile('/index.html');
});

// Listen.
server.listen(port, function() {
	console.log('listening on *:' + port);
});
